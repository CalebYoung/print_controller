import numpy as np, os ,imutils, cv2, png, scipy.misc, sys,time
from PIL import Image

class VisionSystem:
    # Camera Resolution and Cropping Parameters
    Y_top = 90
    Y_bottom = 200
    X_left = 132
    X_right = 132 

    # Select values for HSV color issolation
    lower_H = 143
    upper_H = 160
    lower_S = 0 
    upper_S = 255
    lower_V = 230
    upper_V = 255
    extra_H = 0 # if H range is 150 to 180 AND 0 to 10 set this value to 10 
    min_Contour = 20
    gaussianBlurX = 3
    gaussianBlurY = 3
    
    def __init__(self, Ytop = Y_top, Ybottom = Y_bottom, Xleft = X_left, Xright = X_right, lowerH = lower_H, upperH = upper_H, extraH = extra_H, lowerS = lower_S, upperS = upper_S, lowerV = lower_V, upperV = upper_V, minContour=min_Contour):
        self.Ytop = Ytop
        self.Ybottom = Ybottom
        self.Xleft = Xleft
        self.Xright = Xright
        self.lowerH = lowerH
        self.upperH = upperH
        self.lowerS = lowerS
        self.upperS = upperS
        self.lowerV = lowerV
        self.upperV = upperV
        self.extraH = extraH
        self.minContour = minContour

    def resizeImage(self, img):
        self.img = img
        #trim to only include 17.1mm in X direction and enough Y to fit laser line
        self.img = self.img[self.Ytop : (self.img.shape[0]-self.Ybottom) , self.Xleft:(self.img.shape[1]-self.Xright)  ]
        #compress in X dimension, leaving Y dimension alone
        self.img = cv2.resize(self.img,(128,self.img.shape[0]),interpolation = cv2.INTER_NEAREST)#,interpolation = cv2.INTER_AREA)
        self.original = self.img

    def returnImage(self,destinationFile):
        scipy.misc.imsave(destinationFile, self.img) 

    def convertImagetoHSV(self):
        self.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)

    def gaussianBlur(self):
        self.img = cv2.GaussianBlur(self.img, (VisionSystem.gaussianBlurX, VisionSystem.gaussianBlurY), 0)

    def filterColor(self):

        # bool image of pixels within bounds,convert it to bool numpy
        mainMask = (cv2.inRange(self.img, np.array([self.lowerH, self.lowerS, self.lowerV]), np.array([self.upperH, self.upperS, self.upperV])))

        #if H 180 and H 0 aren't both in range delete this code
        extraH = (cv2.inRange(self.img, np.array([0, self.lowerS, self.lowerV]), np.array([self.extraH, self.upperS, self.upperV])))

        mask = cv2.bitwise_or(mainMask, extraH)	
        self.img = mask
        
    def noSmallContours(self):
        # code to remove small contours
        (_,contours,_) = cv2.findContours(self.img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE) #list of cordinates of edges of countours

        mask = np.ones(self.img.shape[:2], dtype="uint8") #*255

        for contour in contours:
        	area = cv2.contourArea(contour)
        	# add small contours to mask for removal
        	if area < self.minContour:
        		cv2.drawContours(mask, [contour], -1, 0, -1)
        self.img = cv2.bitwise_and(self.img, self.img, mask=mask)

    def visualizeTopLine(self): #returns X Y cordinates of topLine
        temp = np.zeros([self.img.shape[0],self.img.shape[1]])
        for columnIndex in range(self.img.shape[1]):
            for pixelIndex in range(self.img.shape[0]):
                if np.transpose(self.img)[columnIndex][pixelIndex] == 255: #found top of line
                    self.original[pixelIndex,columnIndex] = 0
                    break
	self.img = self.original

    def topLine(self): # returns X Y cordinates of topLine

        y_vals = np.argmax(self.img, axis=0) #locations of top of line
        y_vals = y_vals[y_vals > 0] #eleminate data that isn't on line (doesn't contain a 1) #---------UNCOMMENT ME 
        x_vals = np.arange(y_vals.shape[0])
        pairs = np.concatenate([x_vals[..., np.newaxis], y_vals[..., np.newaxis]], axis=1)
        return pairs

    #code to extract line information from single image	
    def runLine(self, img, destinationFile):
        self.resizeImage(img)
        self.convertImagetoHSV()
        self.gaussianBlur()
        self.filterColor()
        self.noSmallContours()
        self.returnImage(destinationFile)
        self.img = self.topLine()
        return self.img

    def websiteAdaptiveColorFilter(self,img,full_filename):
        print("lowerH :",self.lowerH, "upperH :",self.upperH, "extraH :", self.extraH, "lowerS :", self.lowerS, "upperS :", self.upperS, "lowerV :", self.lowerV, "upperV :", self.upperV)
        self.resizeImage(img)
        self.convertImagetoHSV()
        self.gaussianBlur()
        self.filterColor()
        self.returnImage(full_filename)

        #surfaceArray code
    def generateCorrectiveLayers(self, numpyData):
            
        #find max/min points to generate shape of correctiveLayers
        self.maxV = np.amax(numpyData, axis = 0)
        self.minV = np.amin(numpyData, axis = 0)
        #check for errors
        if ( (self.maxV[0]-self.minV[0]+1) * (self.maxV[1]-self.minV[1]+1) != numpyData.shape[0]):
            print("line isn't continous")
            return 0

	self.correctiveLayers = np.zeros((self.maxV[0]-self.minV[0]+1,self.maxV[1]-self.minV[1]+1,self.maxV[2]-self.minV[2]+1),np.bool)
        #big numbers represent peaks, these require the least ink to be jetted, first correctiveLayer almost nothing is jetted
        for i in range(numpyData.shape[0]): #i is numbers of xyz points in surfaceArray
            self.correctiveLayers[numpyData[i,0],numpyData[i,1]-self.minV[1],(numpyData[i,2]-self.minV[2]):] = 1

    def writeToXYZ(self,numpyData): #added numpyData argument
        #generate new file to write # TODO:
        print("writing to XYZ file, numpyData shape is: ",numpyData.shape)
        xyzFile = open('Directories/outputfile.xyz',"w+")
        indexOfOnes = np.transpose(np.where(numpyData == 1)) #changed from self.correctiveLayers
        for i in range(indexOfOnes.shape):
            xyzFile.write('{0!s} {1!s} {2!s}'.format(indexOfOnes[i][0], indexOfOnes[i][1], indexOfOnes[i][2]))
            xyzFile.write('\n')

    def writeToXYZ2D(self,numpyData):
        xyzFile = open('Directories/xyzOutput/outputfile.xyz',"w+")
        for pixel in numpyData:
            xyzFile.write('{0!s} {1!s} {2!s}'.format(pixel.item(0), pixel.item(1), pixel.item(2)))
            xyzFile.write('\n')

    def saveAsNpLayers(self,Y_row):
        for i in range(self.correctiveLayers.shape[2]):
            print("saveAsNPLayers", self.correctiveLayers.shape)
            np.save(os.path.join(os.getcwd(),'Directories','visionOutput','correctiveLayer_'+'%03d'%i), self.correctiveLayers[...,i])
                 

    def runSurface(self, numpyData, xyzOn=False, Y_row=0):
        # writeToXYZ creates xyz file to open in meshlab (just surface, not layers)
        if(xyzOn):
            self.writeToXYZ2D(numpyData)

        else:
            self.generateCorrectiveLayers(numpyData)
            self.saveAsNpLayers(Y_row)
