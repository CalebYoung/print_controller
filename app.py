from flask import Flask, send_file, render_template, request 
import cv2, time, os, random, picamera, shutil, sys, visionSystemClass

#with picamera.PiCamera as camera:
visionSystem = visionSystemClass.VisionSystem()
piCamPics = os.path.join('static','output_image')
visionOutput = os.getcwd()+'/Directories/visionOutput'

#path for piCamPics
path = os.path.join(os.getcwd(), piCamPics)
try:
	shutil.rmtree(path)
except OSError:
	print("Deletion of the directory %s failed" % path)

try:
	os.mkdir(path)
except OSError:
	print("Creation of the directory %s failed" % path)

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = piCamPics

@app.route('/', methods=['GET', 'POST'])# "/" must match html action
def main():
	full_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'pi_cam_pic.jpg')
	if request.method == 'POST': #request method must match form method in html (could also be get)
		print("request form is", request.form)
		if (request.form.get('generic_button', None)):
		#if 1==1:
			if request.form['generic_button'] == 'Take a Picture':
                                try:
                                        Ytop = int(request.form['Ytop'])
                                except:
                                        Ytop = visionSystem.Ytop
                                try:
                                        Ybottom = int(request.form['Ybottom'])
                                except:
                                        Ybottom = visionSystem.Ybottom
                                try:
                                        Xleft = int(request.form['Xleft'])
                                except:
                                        Xleft = visionSystem.Xleft
                                try:
                                        Xright = int(request.form['Xright'])
                                except:
                                        Xright = visionSystem.Xright
                                print("from inside app.py Ytop: ", Ytop, "Ybottom: ", Ybottom, "Xleft :", Xleft, "Xright :", Xright)
				vision = visionSystemClass.VisionSystem(Ytop=Ytop, Ybottom=Ybottom, Xleft=Xleft, Xright=Xright)

				with picamera.PiCamera() as camera:
                                    camera.resolution = (640,420)
                                    camera.rotation = 180
                                    camera.start_preview()
                                    time.sleep(2)
                                    #camera.sensor_mode = 1
                                    camera.capture(full_filename, format = 'jpeg',use_video_port=True)
                                    camera.close()
                                image = cv2.imread(full_filename)
                                vision.resizeImage(image)
                                vision.returnImage(full_filename)
                        if request.form['generic_button'] == 'Contours Removed':
				try:
					minContour = int(request.form['MinContour'])
				except:
					minContour = visionSystem.minContour
				vision = visionSystemClass.VisionSystem(minContour=minContour)
				with picamera.PiCamera() as camera:
					camera.resolution = (640,420)
                                        camera.rotation = 180
                                        camera.start_preview()
                                        time.sleep(2)
					camera.capture(full_filename, format = 'jpeg',use_video_port=True)
                                        #camera.capture_sequence(['img1.jpg', full_filename, 'img2.jpg'],format = 'jpeg', use_video_port=True)
					camera.close()

				image = cv2.imread(full_filename)
				vision.resizeImage(image)
				vision.convertImagetoHSV()
				vision.gaussianBlur()
				vision.filterColor()
				vision.noSmallContours()
                                vision.returnImage(full_filename)

			if request.form['generic_button'] == 'Laser Line':
				with picamera.PiCamera() as camera:
					camera.resolution = (640, 420)
                                        camera.rotation = 180
                                        camera.start_preview()
                                        time.sleep(2)
					camera.capture(full_filename, format = 'jpeg',use_video_port=True)
					camera.close()
				image = cv2.imread(full_filename)
				visionSystem.resizeImage(image)
				visionSystem.convertImagetoHSV()
				visionSystem.gaussianBlur()
				visionSystem.filterColor()
				visionSystem.noSmallContours()
				visionSystem.visualizeTopLine()
				visionSystem.returnImage(full_filename)

			if request.form['generic_button'] == 'Color Filter':
				try:
					lowerH = int(request.form['lowerH'])
				except:
					lowerH = visionSystem.lowerH
				try:
					upperH = int(request.form['upperH'])
				except:
					upperH = visionSystem.upperH
				try:
					extraH = int(request.form['extraH'])
				except:
					extraH = visionSystem.extraH
				try:
					lowerS = int(request.form['lowerS'])
				except:
					lowerS = visionSystem.lowerS
				try:
					upperS = int(request.form['upperS'])
				except:
					upperS = visionSystem.upperS
				try:
					lowerV = int(request.form['lowerV'])
				except:
					lowerV = visionSystem.lowerV
				try:
					upperV = int(request.form['upperV'])
				except:
					upperV = visionSystem.upperV

                    vision = visionSystemClass.VisionSystem(lowerH=lowerH,upperH=upperH,extraH=extraH,lowerS=lowerS,upperS=upperS,lowerV=lowerV,upperV=upperV)

				with picamera.PiCamera() as camera:
					camera.resolution = (640, 420)
                                        camera.rotation = 180
                                        camera.iso = 100
                                        camera.start_preview()

                                        camera.shutter_speed = camera.exposure_speed
                                        time.sleep(2)
                                        camera.exposure_mode = 'off'
                                        g = camera.awb_gains
                                        camera.awb_mode = 'off'

					camera.capture(full_filename, format = 'jpeg', use_video_port=True)
					camera.close()
				image = cv2.imread(full_filename)
				vision.websiteAdaptiveColorFilter(image,full_filename)

	random_num = random.randint(1,1001)
	full_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'pi_cam_pic.jpg?'+ str(random_num))
	print('returning file to template: ', full_filename)

	return render_template('index.html', output_image = full_filename, resolutionY = 420-visionSystem.Ytop-visionSystem.Ybottom, resolutionX = 640 - visionSystem.Xleft - visionSystem.Xright)

if __name__ == '__main__':
   app.run(host = "0.0.0.0", port = 80, debug = True)
