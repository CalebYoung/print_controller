import argparse,  shutil, os, sys, visionSystemClass, picamera, site, time, serial, numpy as np, PIL.Image as Image, PIL.ImageOps as ImageOps, imutils, cv2
from PIL import Image

camera = picamera.PiCamera()
camera.iso = 100
camera.resolution=(640,420)
camera.rotation = 180

class PiDuino:
    def __init__(self,printWidth = 50):
        self.VisionSystem = visionSystemClass.VisionSystem() 
        self.surface3D = np.empty([0,3],int) # zero thickness 3D scan of surface. List of XYZ points
        self.lineArray = np.empty([0,3],int)
        self.pixelPerLayer = 10 # number of pixels in camera that corispond to one layer thickness 
        self.Dir_piCamPics = os.path.join(os.getcwd(),'Directories','piCamPics')
        self.Dir_visionOutput = os.path.join(os.getcwd(),'Directories','visionOutput')
        self.Dir_visionOutputSupport = os.path.join(os.getcwd(),'Directories','visionOutputSupport')
        self.Dir_input = os.path.join(os.getcwd(),'Directories','input')
        self.Dir_xyzOutput = os.path.join(os.getcwd(),'Directories','xyzOutput')
        self.Dir_sliceNumpys = os.path.join(os.getcwd(),'Directories','sliceNumpys')
        self.Dir_support = os.path.join(os.getcwd(),'Directories','support')
        self.Dir_visualizeNumpys = os.path.join(os.getcwd(),'Directories','visualizeNumpys')
        self.Dir_testImages = os.path.join(os.getcwd(),'Directories','testImages')
        self.inputSorted, self.numOfLayers = self.inputSorted()
        self.sizeOfTopline = 0
        
    def flushDirectories(self, directory):
        try:
            shutil.rmtree(directory)
        except OSError:
            print("Deletion of the directory %s failed" % directory)
        try:
            os.mkdir(directory)
        except OSError:
            print("Creation of the directory %s failed" % directory)

    def inputSorted(self):
        #assumes the following file naming convention:'out0006.png'
        unsortedFiles = os.listdir(self.Dir_input)
        for file in unsortedFiles:
            if file[0]=='.':
                unsortedFiles.remove(file)
        return(sorted(unsortedFiles,key=lambda x: int(x[3:7])),len(unsortedFiles))

    def imageConverter(self,printWidth, layerThickness, cropBorder, argsNew):
        for i in range(self.numOfLayers):
            # import Image
            PILimage = Image.open(os.path.join(self.Dir_input,self.inputSorted[i]))
            # PIL to turn it into black and white with 1, and LA for grayscale
            PILimage = PILimage.convert('1')
            # Rotate
            PILimage = PILimage.transpose(Image.ROTATE_90)
            # Crop edges
            PILimage = ImageOps.crop(PILimage, cropBorder)
            # initiate image size on first layer

            if i < 1:
                #determine width of actual part (no support, no extra zeros to make divisible by 128
                widthOfJustImgData = int(7.3035 * printWidth) #nozzels per mm
                #number of rows needed to cover image + support
                self.Y_rows=((widthOfJustImgData+layerThickness*2)-1)/128+1
                self.widthInPixels = 128*self.Y_rows
                self.lengthInPixels = int(PILimage.size[1]/float(PILimage.size[0])*self.widthInPixels) + layerThickness*2
                print("Pi| total number of layers to be printed: %s" %(self.numOfLayers))
                print("Pi| image pixels: %s width, %s length - just image data" %(widthOfJustImgData,self.lengthInPixels-layerThickness*2))
                print("Pi| image pixels: %s width, %s length - including support thickness" %(widthOfJustImgData+layerThickness,self.lengthInPixels))
                print("Pi| image pixels: %s width, %s length - width divsible by 128" %(self.widthInPixels,self.lengthInPixels))
                print("Pi| image pixels: %s width, %s length - final size (including offsetting)" %(self.widthInPixels+128,self.lengthInPixels+128))
                print("Pi| print will be %s rows, counting extra row for offsetting" %(self.Y_rows+1))
                if not argsNew: return None
                self.flushDirectories(self.Dir_sliceNumpys)

            PILimage = PILimage.resize((widthOfJustImgData,self.lengthInPixels-layerThickness*2), Image.ANTIALIAS)
            # place image in template of zeros to make room for support and extra zeros to make width divisible by 128 
            correctedWidthArray = np.zeros((self.lengthInPixels,self.widthInPixels),dtype=bool)
            correctedWidthArray[layerThickness:self.lengthInPixels-layerThickness,  layerThickness:widthOfJustImgData+layerThickness]=np.array(PILimage) 
            #  Convert To Numpy Array and save
            np.save(os.path.join(self.Dir_sliceNumpys+'/slice%03d'%i), np.array(correctedWidthArray))
        print("Pi| converted %s images"%self.numOfLayers)

    def support(self,supportWidth):
        template = np.zeros((self.lengthInPixels,self.widthInPixels),dtype=bool)
        # start looping through in reverse layers
        for i in reversed(range(self.numOfLayers)):
            #load layers in reverse
            npSlice = np.load(self.Dir_sliceNumpys+'/slice%03d.npy'%(i))

            #update template
            it = np.nditer(npSlice, flags=['multi_index']) #turns layer array into iterable list with multidimensional indexes
            while not it.finished:
                if(it[0] == 1): #if a 1 is found
                    #check to see if its on the edge of the solid
                    M=it.multi_index
                    if (all(npSlice[M[0]-1,: M[1]-1:M[1]+2]) and all(npSlice[M[0]: M[0]+2]) and all(npSlice[M[0]+1: M[0]+2])):
                        template[M]=1
                    #on edge: add expanded boarder to template
                    else:
                        template[M[0]-supportWidth:M[0]+supportWidth+1,M[1]-supportWidth:M[1]+supportWidth+1] = 1
                it.iternext()

            #generateSupport - when the layer = 1, return zero. Else, refer to the template
            supportArray = np.where(npSlice==1, 0, template)
            
            np.save((self.Dir_support+'/supportSlice_%03d'%i),supportArray)


    def scan(self, ArduinoMega): #rows, length, image cropping information
        self.flushDirectories(self.Dir_piCamPics)
        self.flushDirectories(self.Dir_testImages)
        for i in range(self.Y_rows):
            os.mkdir(os.path.join(self.Dir_piCamPics,'row_'+str(i)))
            os.mkdir(os.path.join(self.Dir_testImages,'row_'+str(i)))

        #let arduino know you want to do a scan
        print("Pi| writing 'vision' to Mega")
        ArduinoMega.write(bytes("v")+'\x55')
        # import image
        X_row = 0
        Y_row = 0
        for Y_row in range(self.Y_rows):
            #handshake code
            while(True):
                if (ArduinoMega.read(1)=="P"):
                    print("Pi| received 'P' from Mega, taking pictures")
                    camera.start_preview()
                    camera.shutter_speed = camera.exposure_speed
                    time.sleep(2)
                    camera.exposure_mode = 'off'
                    g = camera.awb_gains
                    camera.awb_mode = 'off'
                    break
            ArduinoMega.write('\x00') #Pi ready: start scan!
            print("Mega|%s"%(ArduinoMega.readline()))
            # capture imagess
            camera.capture_sequence((self.Dir_piCamPics+'/row_'+str(Y_row)+'/line_%d.jpg' %i for i in range(self.lengthInPixels/2)), format = 'jpeg',use_video_port = True) #------delete /10 in range 
            
    def generateSurface3D(self):	
        print("Pi| Processing Scan")
        #loop through all the images, extract line data and compile them into surface array
        self.flushDirectories(self.Dir_visionOutput)
        self.flushDirectories(self.Dir_xyzOutput)
        for Y_row in range(len(os.listdir(self.Dir_piCamPics))):
            for i in range(len(os.listdir(self.Dir_piCamPics+'/row_'+str(Y_row)))): #loop through all lines in row
                image = cv2.imread(self.Dir_piCamPics+'/row_'+str(Y_row)+'/line_'+str(i)+'.jpg')
                #print("piDuino.generateSurface3D - piCamimage shape is: ", image.shape)
                self.lineArray = np.insert(self.VisionSystem.runLine(image,self.Dir_testImages+'/row_'+str(Y_row)+'/line_'+str(i)+'.jpg'),0, i, axis =1)
                #self.Dir_testImages+'/row_'+str(Y_row)+'/line_'+str(i)+'.jpg')
                self.sizeOfTopline += self.lineArray.shape[0]
                self.lineArray[:,1]+=Y_row*128 # scoot over line based on Y_row
                self.surface3D = np.append(self.surface3D, self.lineArray, axis=0) # add lines to form thin surface
                #print("piDuino.generateSurface3D surface3D shape: ", self.surface3D.shape)
        print("Pi| Scan Coverage: %s percent"%(float(self.sizeOfTopline*400)/float(self.widthInPixels*self.lengthInPixels)))
        print("Pi| piDuino.processScan: end result of surface array", self.surface3D.shape)
        self.flushDirectories(self.Dir_piCamPics)

    #correctiveLayer size should equal full scanning bed, regardless of whether laser is detected
    def generateCorrectiveLayers(self,layerThickness,sliceLayerNum,numLayersToCorrect):
        #calculate highest layer "effective Current Layer"
        self.maxV = np.amax(self.surface3D, axis = 0) #x, y, z
        self.minV = np.amin(self.surface3D, axis = 0)
        self.heighestScanPoint = self.maxV[2] # * pixelPerLayer + Zmotor movememnt*layerNum (in microns)
        self.effectiveCurrentLayer = self.heighestScanPoint/layerThickness

        #remove everything that doesn't pertain to the top layers
        self.surface3D = self.surface3D[self.surface3D[:,2]> (self.maxV[2]- self.pixelPerLayer*numLayersToCorrect)]

        #generates corrective layer "full" cube of ones for top 20 layers
        self.correctiveLayers = np.ones((self.lengthInPixels,self.Y_rows*128,numLayersToCorrect),np.bool)


        print("Pi| correctiveLayers shape: ", self.correctiveLayers.shape)
        print("Pi| Surface3D max: ", self.maxV, "min: ", self.minV)

        #use XYZ data in surface3D to generate 3D binary corrective Layers
        for i in range(self.surface3D.shape[0]): #i is numbers of xyz points in surfaceArray
            self.correctiveLayers[self.surface3D[i,0],self.surface3D[i,1],:(self.surface3D[i,2]-self.maxV[2]+numLayersToCorrect)] = 0
            #for x, y cordinate in xyz point list make everything below the Z point zero (minus the amount removed (total layers -20 layers))

        print("Pi| piDuino.generateCorrectiveLayers - correctiveLayer in final form: ", self.correctiveLayers.shape)
        for i in range(numLayersToCorrect):
            sliceLayer = np.load(os.path.join(self.Dir_sliceNumpys,'slice%03d'%(sliceLayerNum-numLayersToCorrect+i)+'.npy'))
            sliceLayerSupport = np.load(os.path.join(self.Dir_support,'supportSlice_%03d'%(sliceLayerNum-numLayersToCorrect+i)+'.npy'))
            print("Pi| sliceLayer: %d %d"%(sliceLayer.shape))
            bitwiseSlice = np.bitwise_and(self.correctiveLayers[...,i],sliceLayer)
            bitwiseSliceSupport = np.bitwise_and(self.correctiveLayers[...,i],sliceLayerSupport)
            np.save((self.Dir_visionOutput+'/visionSlice_%02d'%i),bitwiseSlice)
            np.save((self.Dir_visionOutputSupport+'/visionSlice_%02d'%i),bitwiseSliceSupport)

    def offsetNumpyArray(self,numpySlice,absLayerNum):
        #no offsetting: image is in top left of block
        offset_list = [0, 36, 55, 126, 33, 65, 84, 60, 24, 62, 35, 104, 43, 70, 110, 73, 91, 3, 64, 114, 71, 103, 75, 113, 58, 80, 83, 95, 6, 13, 97, 117,
                        51, 86, 23, 59, 12, 37, 81, 31, 88, 101, 50, 67, 8, 17, 48, 123, 38, 120, 90, 74, 72, 79, 87, 66, 20, 121, 56, 26, 109, 78, 99, 4,
                        82, 29, 5, 16, 2, 39, 89, 49, 9, 108, 106, 54, 44, 19, 125, 11, 94, 122, 45, 76, 25, 30, 57, 22, 61, 53, 107, 105, 42, 41, 127, 124,
                        47, 96, 111, 92, 77, 118, 100, 32, 112, 15, 40, 63, 28, 34, 1, 46, 93, 115, 68, 14, 119, 10, 7, 27, 102, 116, 52, 18, 21, 98, 69, 85]
        offset = 128 - offset_list[absLayerNum%128] #returns offset number
        numpySliceOffset = np.zeros((numpySlice.shape[0]+128,numpySlice.shape[1]+128),dtype=bool) #create blank block the size of the layer to be printed
        numpySliceOffset[offset:offset+numpySlice.shape[0], offset:offset+numpySlice.shape[1]] = numpySlice
        return numpySliceOffset

    def numpyToRAW(self, numpyPrintPass):
        # Clean out all header information of the array and return bytes
        return np.packbits(numpyPrintPass, axis=-1)

    #  saves numpy array as a PNG in self.Dir_visualizeNumpys
    def numpyToPNG(self,directory=False,numpyName=False,numpy=False):
        if directory:
            for npFile in os.listdir(directory):
                numpyArray = np.load(directory+'/'+npFile)
                databytes = np.packbits(numpyArray, axis=1)
                PILimage = Image.frombytes(mode='1', size=numpyArray.shape[::-1], data=databytes)
                PILimage.save(os.path.join(self.Dir_visualizeNumpys,npFile[:-4]+'.png'))
        else:
            databytes = np.packbits(numpy, axis=1)
            PILimage = Image.frombytes(mode='1', size=numpy.shape[::-1], data=databytes)
            PILimage.save(os.path.join(self.Dir_visualizeNumpys,numpyName+'.png'))

