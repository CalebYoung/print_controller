import argparse,  shutil, os, sys, visionSystemClass, imutils, picamera, site,  serial, cv2, piDuino, struct, numpy as np
from PIL import Image
np.set_printoptions(threshold=sys.maxsize)

# initiate Variables 
layerThickness = 0 #in microns
numLayersToCorrect = 1 # how many layers vision system corrects
printWidth = 40 # in mm 
scanFreq = 2 # scan every nth layer
cropBorder = (240,10,240,10) #left, upper, right, lower
sliceLayerNum = 1 # current layer from sliced files
absLayerNum = 1 # total number of layers printed
arduinoZero1 = False # is connected to print driver #1
arduinoZero2 = False # is connected to print driver #2
arduinoMega = False  # is connected to motor/pressure system controller

# initiate class libraries
piDuino = piDuino.PiDuino(printWidth) # Class to handle various functions that suppliment main.py
VisionSystem = visionSystemClass.VisionSystem() #opens class to write into this directory

#turn on to generate support material
#piDuino.support(1) 

#initiate ArgumentParser
ap = argparse.ArgumentParser()
ap.add_argument("-x", "--xyz", action = "store_true", help = "Generates .xyz file instead of numpy layers")
ap.add_argument("-i", "--img", action = "store_true", help = "Generates png images to view in visualizeNumpys Directory")
ap.add_argument("-n", "--new", action = "store_true", help = "Runs image converter, use when running new print files")
args = ap.parse_args()
if args.xyz:
    print("Pi| xyz file on the way")
if args.img:
    print("Pi| pngs on the way")
    piDuino.flushDirectories(piDuino.Dir_visualizeNumpys)
    piDuino.numpyToPNG(piDuino.Dir_sliceNumpys)
    piDuino.numpyToPNG(piDuino.Dir_support)
piDuino.imageConverter(printWidth,layerThickness,cropBorder,args.new)


################ 1) Connect to arduinos #################
#initiate Arduinos
try:
    ArduinoZero1=serial.Serial("/dev/serial/by-id/usb-Arduino_LLC_Arduino_MKRZero_3E7FB4AF5150484347202020FF0F281E-if00",9600,timeout=10)
    print("Zero1| %s" %ArduinoZero1.readline())
    arduinoZero1 = True
except:
    print("Pi| Zero 1 not connected") 

try:
    ArduinoZero2=serial.Serial("/dev/serial/by-id/usb-Arduino_LLC_Arduino_MKRZero_998448B851504843472020F1A1EdWOT WORKd-if00",9600,timeout=3)
    print("Zero2| %s" %ArduinoZero1.read_until().strip())
    arduinoZero2 = True
except:
    print("Pi| Zero 2 not connected") 

try:
    ArduinoMega=serial.Serial("/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0",9600,timeout=3)
    print("Mega| %s" %ArduinoMega.readline())
    arduinoMega = True
except:
    print("Pi| Mega not connected") 

################ 2) initilize Variables to arduino #################
#find shape of first array and send info to arduino
if arduinoZero1:
    ArduinoZero1.write(bytes(str(piDuino.lengthInPixels+128))+'\x55')
    print("Zero1| %s" %ArduinoZero1.readline())

if arduinoZero2:
    ArduinoZero2.write(bytes(str(piDuino.lengthInPixels+128))+'\x55')
    print("Zero2| %s" %ArduinoZero2.readline())

if arduinoMega:
    #structures Y_rows into ASCII value for arduino
    ArduinoMega.write(struct.pack('>B',piDuino.Y_rows+1))
    print("Mega| %s" %ArduinoMega.readline())

#######################  3) Master Print Loop  ######################
print('Pi| Entering Master Loop')
while True:

    #SCAN
    if sliceLayerNum % scanFreq == 0 or sliceLayerNum == piDuino.numOfLayers: # if time to scan or last layer ]
        print("Pi| -------------------SCAN-------------------")
        piDuino.scan(ArduinoMega)
        print("Pi| ----------------SCAN COMPLETE----------------")
        piDuino.generateSurface3D()
        if args.xyz:
            print("Pi| preparing XYZ files")
            VisionSystem.writeToXYZ2D(piDuino.surface3D)
        piDuino.generateCorrectiveLayers(layerThickness,sliceLayerNum,numLayersToCorrect) # exports numpys to piDuino.Dir_visionOutput
        if args.img: piDuino.numpyToPNG(piDuino.Dir_visionOutput)

        #print all corrective layers from scan
        for i in range(len(os.listdir(piDuino.Dir_visionOutput))):
            currentNumpyLayer = np.load(piDuino.Dir_visionOutput+'/visionSlice_%02d.npy'%i)
            offsetNumpy = piDuino.offsetNumpyArray(currentNumpyLayer, absLayerNumi - 1)
            
            #break numpy into rows
            layerPrintPasses = np.split(offsetNumpy,piDuino.Y_rows+1,axis=1)
            for printPass in range(piDuino.Y_rowsi + 1):
                # visalize PNGs
                if args.img: piDuino.numpyToPNG(numpyName = ('visionSlice%03d_row%d'%(absLayerNum-sliceLayerNum,printPass)),numpy = layerPrintPasses[printPass])
                # prepare RAW data
                RAWdata = piDuino.numpyToRAW(layerPrintPasses[printPass])

                # Send Data to Zero based on request from Mega 
                while True:
                    print("Pi|(vision) waiting for request from Mega")
                    if ArduinoMega.readline() == "request from Mega to fire\r\n":
                        if arduinoZero1:
                            ArduinoZero1.write(RAWdata) 
                            ArduinoZero1.write('\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55') # end line code
                        else: print("Pi| Arduino would print now....if it existed")
                        break
                #-----Pi FIRE-----
                ArduinoMega.write("f"+'\x55')
                print("Pi| FIRING(vision mode): slice%03d_row%s)"%(sliceLayerNum,printPass))
            absLayerNum += 1
    
    #NORMAL PRINTING CYCLE (as opposed to scan)
    currentNumpyLayer = np.load(piDuino.Dir_sliceNumpys+'/slice%03d'%sliceLayerNum+'.npy')
    offsetNumpy = piDuino.offsetNumpyArray(currentNumpyLayer, absLayerNum-1) # -1 because absLayerNum needs to start at 1 so there is no X % 0 

    #break numpy into rows
    layerPrintPasses = np.split(offsetNumpy,piDuino.Y_rows+1,axis=1)
    for printPass in range(piDuino.Y_rows+1):
        # visalize PNGs
        if args.img: piDuino.numpyToPNG(numpyName = ('Layer%03d_row%d'%(sliceLayerNum,printPass)),numpy = layerPrintPasses[printPass])
        # prepare RAW data
        RAWdata = piDuino.numpyToRAW(layerPrintPasses[printPass])
        # Send Data to Arduino Zero based on request from Mega        
        while True:
            print("Pi| waiting for request from Mega")
            if ArduinoMega.readline() == "request from Mega to fire\r\n":
                if arduinoZero1:
                    print("Pi| writing RAW data to Zero. Shape: %s %s"%(RAWdata.shape))
                    ArduinoZero1.write(RAWdata) 
                    ArduinoZero1.write('\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55') # end line code
                else: print("Pi| Arduino would print now....if it existed")
                break

        #-----Pi FIRE-----
        ArduinoMega.write("f"+'\x55')
        print("Pi| FIRING(normal mode): slice%03d_row%s"%(sliceLayerNum,printPass))
        if arduinoZero1: print("Zero| Print Summary: %s"%(ArduinoZero1.readline()))
    absLayerNum += 1
    sliceLayerNum += 1
    
    #EXIT
    if sliceLayerNum == piDuino.numOfLayers:
        break
print("Pi| FINISHED! sliceLayerNum: ",sliceLayerNum, "absLayerNum: ", absLayerNum)

