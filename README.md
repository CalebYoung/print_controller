# Avana's Multimaterial Inkjet 3D Printer Overview #

![](readme_images/scan.gif)

This is a prototype for the first lowcost inkjet 3D printer. Inkjet 3D printing is the most accurate type of multimaterial printing. It is a non-contact, open loop form of printing. This causes the part to build up unevenly after several layers.
Typical inkjet 3D printers cut material off after every pass to keep the service accurate. This wastes material and adds cost and complexity to the printer.

## Why is this prototype different?
This 3D printing prototype scans the surface of the part being built, then generates and prints corrective
layers flatten out the surface. This closes the printing loop cheaply, with no wasted material or extra moving parts added to the printer.

## Repository Overview
**Main.py**: master file that controls what is printed and when. Sends data to print drivers and firing commands to motor drivers.
```
python Main.py --xyz --img --new

--xyz: Generates .xyz file to visualize 3D scan
--img: Saves the post processed images used by the scanning system for debugging purposes
--new: Runs image converter on imput files, use when running new print files
```
**VisionSystem.py**: Library to process individual images used in vision system. Utilizes OpenCV and Numpy libraries. Extracts laser line data from images, and outputs numpy array.

**PiDuino.py**: Library that contains all other functions for the printer:

  * *Slicer*- Converts input files (PNGs) to binary numpy arrays ready to be fed into the printdrivers

  * *Support Generation*- Generates support material files needed to encapsulate printed part. Support material is washed away after printing.

  * *Offset code*- Offsets all images a random amount of nozzel widths between 0-128. The motors mirror this offsetting to insure printheads will always take a different path across the build surface. This insures any problems from clogged nozzels are evenly spread over the entire build surface

  * *Scan*- Controls communication with motor drivers and camera for scan sequence
 
  * *Scan post-processing*- Takes arrays generated from the VisionSystem library, forms a 3D reconscrution of the surface and then generates print layers need to correct inaccuracies in the surface.

## Vision System Overview

Every nth print layer (10-50) a “scanning sequence” is initiated. The scanning sequence follows the same path as the printing sequence; one 
pass in the X direction for every 128 voxels in the Y direction (thus scanning across the entire print bed in the same path in which you would 
mow your lawn). During the scan a raspberry pi camera mounted at a 45% angle takes an image for every voxel in the Y direction. After all of these 
images are collect they are post processed. The width of the images are cut to 128 pixels to correspond with the number of nozzels. The top of 
the laser line is identified with openCV methods, the height of each pixel is placed into an array. All of these arrays are combined to make a 
3D reconstruction of the print surface. From there correction layers are generated to fill in all the low parts of the surface, resulting in a 
flat, even surface.

Visualization of software locating laser location to sub 100 micron resolution (black line overlaid on image)

<p float="center">
<img src="readme_images/tree_scan.png" height="225">
<img src="readme_images/tree_scale.png" height="225">
</p>

**laser line location extracted from hundreds of images and pasrsed into XYZ file for viewing**

![](readme_images/Tree_xyz.gif)

## Printed parts! 

| | | |
|:-------------------------:|:-------------------------:|:-------------------------:|
|<img width="500" alt="glasses" src="readme_images/glasses_1.png">  glasses with lens in single print |  <img width="500" alt="lens" src="readme_images/glasses_2.png"> view through lens|
|<img width="500" alt="glasses" src="readme_images/conductive_traces.png">  embedded conductive traces |  <img width="500" alt="lens" src="readme_images/logo_2.png"> high resolution multi-material logo|

